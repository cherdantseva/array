var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0
];

console.log("Задание 1");
for (var i = 0; i < studentsAndPoints.length; i += 2) {
    var ball = i + 1;
    console.log('Студент ' + studentsAndPoints[i] + ' набрал ' + studentsAndPoints[ball] + ' баллов');
}

console.log("Задание 2");
var maxBall = -1,
    maxName;

for (i = 0; i < studentsAndPoints.length; i += 2) {
    if (studentsAndPoints[i + 1] >= maxBall) {
        maxBall = studentsAndPoints[i + 1];
        maxName = studentsAndPoints[i];
    }
}

console.log('Студент, набравший максимальный балл:');
console.log('Студент ' + maxName + ' имеет максимальный балл ' + maxBall);

console.log("Задание 3");

studentsAndPoints.push('Николай Фролов', 0);
studentsAndPoints.push('Олег Боровой', 0);

console.log(studentsAndPoints);

console.log("Задание 4");


if (studentsAndPoints.indexOf('Антон Павлович') >= 0) {
    studentsAndPoints[studentsAndPoints.indexOf('Антон Павлович') + 1] +=10;
}

if (studentsAndPoints.indexOf('Николай Фролов') >= 0) {
    studentsAndPoints[studentsAndPoints.indexOf('Николай Фролов') + 1] +=10;
}

// Закомментированный способ, приведенный ниже, гораздо лучше.
// Однако, на момент выполнения задания мы не должны были знать функции, поэтому см. выше :)
//function addPoints(name, point) {
//    var index = studentsAndPoints.indexOf(name);
//    if (index >= 0) {
//        studentsAndPoints[index + 1] += point;
//    }
//}
//
//addPoints('Антон Павлович', 10);
//addPoints('Николай Фролов', 10);


console.log(studentsAndPoints);

console.log("Задание 5");

console.log('Студенты, не набравшие баллов:');
for (i = 0; i < studentsAndPoints.length; i += 2) {
    if (studentsAndPoints[i + 1] == 0) {
        console.log(studentsAndPoints[i]);
    }
}

console.log("Задание 6");

for (i = 0; i < studentsAndPoints.length; i += 2) {
    if (studentsAndPoints[i + 1] == 0) {
        studentsAndPoints.splice(i, 2);
        i -= 2
    }
}

console.log(studentsAndPoints);